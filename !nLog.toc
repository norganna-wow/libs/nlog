## Title: nLog |cff224477(lib)
## Notes: Debugging console to display debugging messages easily.
##
## Interface: 100206
## SavedVariables: nLogData
## OptionalDeps: !Swatter, SlideBar, LibDataBroker
## IconTexture: Interface/Addons/!nLog/Textures/nLogIcon.blp
##
## Version: <%version%> (<%codename%>)
## Revision: $Id$
## Author: Norganna's AddOns
## X-Part-Of: Libraries
## X-Category: Library
## X-URL: http://auctioneeraddon.com/
## X-Feedback: https://auctioneeraddon.com/slack
##
Support\Load.xml
nLog.lua
